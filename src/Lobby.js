import React from 'react';
import { PayPalButton } from "react-paypal-button-v2";

const Lobby = ({
  username,
  handleUsernameChange,
  roomName,
  handleRoomNameChange,
  handleSubmit
}) => {
  return (
    <form onSubmit={handleSubmit}>
      <h2>Enter a room</h2>
      <div>
        <label htmlFor="name">Name:</label>
        <input
          type="text"
          id="field"
          value={username}
          onChange={handleUsernameChange}
          required
        />
      </div>

      <div>
        <label htmlFor="room">Room name:</label>
        <input
          type="text"
          id="room"
          value={roomName}
          onChange={handleRoomNameChange}
          required
        />
      </div>

      <button style={{marginBottom: '32'}} type="submit">Submit</button>

      <div>
        <PayPalButton
          amount="1"
          // shippingPreference="NO_SHIPPING" // default is "GET_FROM_FILE"
          onSuccess={(details, data) => {
            alert("Transaction completed by " + details.payer.name.given_name);
            console.log('transaction ', details, 'data ', data)

            // OPTIONAL: Call your server to save the transaction
            // return fetch("/paypal-transaction-complete", {
            //   method: "post",
            //   body: JSON.stringify({
            //     orderId: data.orderID
            //   })
            // });
          }}
          options={{
            clientId: process.env.PAYPAL_SANDBOX_CLIENT_ID || 'AQnd2k4l-xxBlqPVeWPM2zHLeYzFbdpVSDomED7pInlSc0e5qouqc0UJsHHXNTcSKcZd27ku9FXBrPb3'
          }}
        />
      </div>
    </form>
  );
};

export default Lobby;
